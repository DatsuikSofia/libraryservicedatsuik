package com.epam.bo;

import com.epam.dao.BookDAO;
import com.epam.model.Book;

import java.util.List;

public class BookBO {

    public List<Book> getAllBooks() {
        return BookDAO.getAllBooks();
    }

    public boolean takeNewBook(Book book) {
        return BookDAO.addBook(book);
    }

    public Book getBook(String title) {
        return BookDAO.getBookByTitle(title);
    }

    public List<Book> getBooksByAuthor(String author) {
        return BookDAO.getBooksByAuthor(author);
    }

    public boolean deleteBook(String author, String title) {
        return BookDAO.changeBook(title, author);
    }
}
