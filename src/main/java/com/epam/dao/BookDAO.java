package com.epam.dao;


import com.epam.model.Book;
import com.epam.utils.JacksonParser;
import com.epam.utils.PropertyParser;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class BookDAO {

    private static Logger LOG = Logger.getLogger(BookDAO.class);
    private static PropertyParser property = new PropertyParser();
    private static JacksonParser parser = new JacksonParser();

    public static List<Book> getAllBooks() {
        return parser.readBooks(new File(property.getProperty("file")));
    }

    public static boolean addBook(Book book) {
        LOG.info("addBook method");
        List<Book> books = new ArrayList(getAllBooks());
        boolean isAdded;
        if (!books.contains(book)) {
            books.add(book);
            parser.writeBooks(books, new File(property.getProperty("file")));
            isAdded = true;
        } else {
            isAdded = false;
        }
        return isAdded;
    }

    public static Book getBookByTitle(String title) {
        List<Book> books = new ArrayList(getAllBooks());
        Book myBook = null;
        for (Book book : books) {
            if (book.getTitle().equals(title)) {
                myBook = book;
            }
        }
        return myBook;
    }

    public static List<Book> getBooksByAuthor(String author) {
        List<Book> books = new ArrayList(getAllBooks());
        List<Book> authorBooks = books.stream()
                .filter(book -> author.equals(book.getAuthor())).collect(Collectors.toList());
        if (authorBooks.size() < 5) {
            return books;
        }
        if (authorBooks.size() == 5) {
            return authorBooks;
        }
        return authorBooks;
    }

    public static boolean changeBook(String title, String author) {
        LOG.info("changeBook method");
        List<Book> books = new ArrayList(getAllBooks());
        boolean changed = false;
        if (books.stream() != null) {
            books.removeIf(book -> (book.getTitle().equals(title) && book.getAuthor().equals(author)));
            parser.writeBooks(books, new File(property.getProperty("file")));
            changed = true;
        } else {
            changed = false;
        }
        return changed;
    }
}

