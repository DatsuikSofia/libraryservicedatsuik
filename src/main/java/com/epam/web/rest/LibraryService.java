package com.epam.web.rest;

import com.epam.model.Book;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/libraryREST")
public interface LibraryService {

    @GET
    @Path("/books")
    @Produces("application/json; charset=UTF-8")
    Response getAllBooks();

    @GET
    @Path("/book/{title}")
    @Consumes("text/plain; charset=UTF-8")
    @Produces("application/json; charset=UTF-8")
    Response getBook(@PathParam("title") String title);

    @GET
    @Path("/books/authors/{author}")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    Response getBooksByAuthor(@PathParam("author") String author);

    @POST
    @Path("/book")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response takeBook(Book book);

    @DELETE
    @Path("/book")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response deleteBook(String title, String author);
}
