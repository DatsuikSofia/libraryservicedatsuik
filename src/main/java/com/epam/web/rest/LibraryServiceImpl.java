package com.epam.web.rest;

import com.epam.bo.BookBO;
import com.epam.exeption.ErrorMessage;
import com.epam.exeption.RecordNotFoundException;
import com.epam.model.Book;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Objects;

public class LibraryServiceImpl implements LibraryService {
    private static final Logger LOG = Logger.getLogger(LibraryServiceImpl.class);

    @Override
    public Response getAllBooks() {
        LOG.info("getAllBooks method");
        BookBO bookBO = new BookBO();
        return Response.ok().entity(bookBO.getAllBooks()).build();
    }

    @Override
    public Response getBook(String title) {
        LOG.info("getBook method");
        Response response;
        BookBO bookBO = new BookBO();
        Book book = bookBO.getBook(title);
        if (!Objects.isNull(book)) {
            return Response.ok().entity(book).build();
        } else {
            RecordNotFoundException record = new RecordNotFoundException(ErrorMessage.NO_BOOK_WITH_THIS_TITLE, book.getTitle().equals(title));
            LOG.warn(record.getMessage());
            response = Response.status(Response.Status.NOT_FOUND).entity(record).build();
        }
        return response;
    }

    @Override
    public Response getBooksByAuthor(String author) {
        LOG.info("getBooksByAuthor method");
        Response response;
        BookBO bookBO = new BookBO();
        List<Book> books = bookBO.getBooksByAuthor(author);
        if (!books.isEmpty()) {
            return Response.ok().entity(books).build();
        } else {
            RecordNotFoundException record = new RecordNotFoundException(ErrorMessage.NOT_ENOUGH_BOOKS_OF_AUTHOR, books.size());
            LOG.warn(record.getMessage());
            response = Response.status(Response.Status.NOT_FOUND).entity(record).build();
        }
        return response;
    }

    @Override
    public Response takeBook(Book book) {
        LOG.info("takeBook method");
        Response response;
        BookBO bookBO = new BookBO();
        if (bookBO.takeNewBook(book)) {
            response = Response.ok().build();
        } else {
            RecordNotFoundException record = new RecordNotFoundException(ErrorMessage.ELECT_BOOK_ALREADY_EXIST, book.getTitle());
            LOG.warn(record.getMessage());

            response = Response.status(Response.Status.NOT_ACCEPTABLE).entity(record).build();
        }
        return response;
    }

    @Override
    public Response deleteBook(String title, String author) {
        LOG.info(" deleteBook method");
        Response response;
        BookBO bookBO = new BookBO();

        if (!bookBO.deleteBook(title, author)) {
            RecordNotFoundException record = new RecordNotFoundException(ErrorMessage.NO_BOOK_THIS_AUTHOR_WITH_THIS_TITLE);

            LOG.warn(record.getMessage());
            response = Response.status(Response.Status.NOT_FOUND).entity(record).build();
        } else {
            response = Response.ok().build();
        }
        return response;
    }


}
