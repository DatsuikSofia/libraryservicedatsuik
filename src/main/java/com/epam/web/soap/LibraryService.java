package com.epam.web.soap;


import com.epam.exeption.ServiceException;
import com.epam.model.Book;

import javax.jws.WebService;
import java.util.List;

@WebService
public interface LibraryService {
    List<Book> getAllBooks();

    Book getBook(String title) throws ServiceException;

    boolean deleteBook(String title, String author);

    List<Book> getBooksByAuthor(String authorName);

    boolean takeBook(Book book);
}
