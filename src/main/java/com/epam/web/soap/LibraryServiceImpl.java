package com.epam.web.soap;

import com.epam.bo.BookBO;
import com.epam.exeption.ErrorMessage;
import com.epam.exeption.RecordNotFoundException;
import com.epam.exeption.ServiceException;
import com.epam.model.Book;
import org.apache.log4j.Logger;

import javax.jws.WebService;
import java.util.List;
import java.util.Objects;

@WebService(endpointInterface = "com.epam.web.soap.LibraryService")

public class LibraryServiceImpl implements LibraryService {

    private Logger LOG = Logger.getLogger(LibraryService.class);

    @Override
    public List<Book> getAllBooks() {
        LOG.info("getAllBooks method");
        BookBO bookBO = new BookBO();
        return bookBO.getAllBooks();
    }

    @Override
    public Book getBook(String title) throws ServiceException {
        LOG.info("getBook method");
        BookBO bookBO = new BookBO();
        Book book = bookBO.getBook(title);
        if (Objects.isNull(book)) {
            RecordNotFoundException record = new RecordNotFoundException(ErrorMessage.NO_BOOK_WITH_THIS_TITLE, book.getTitle().equals(title));
            LOG.warn(record);
              throw new ServiceException(record);
        }
        LOG.info("getBook result:" + book);
        return book;
    }

    @Override
    public boolean deleteBook(String title, String author) {
        LOG.info("deleteBook method");
        BookBO bookBO = new BookBO();
        if (!bookBO.deleteBook(author, title)) {
            RecordNotFoundException record = new RecordNotFoundException(ErrorMessage.NO_BOOK_THIS_AUTHOR_WITH_THIS_TITLE);
            LOG.warn(record.getMessage());
            //  throw new ServiceException(record);
        }
        LOG.info("deleteBook result:" + true);
        return true;
    }

    @Override
    public List<Book> getBooksByAuthor(String author) {
        LOG.info("getBooksByAuthor method");
        BookBO bookBO = new BookBO();
        List<Book> books = bookBO.getBooksByAuthor(author);
        if (books.isEmpty()) {
            RecordNotFoundException record = new RecordNotFoundException(ErrorMessage.NOT_ENOUGH_BOOKS_OF_AUTHOR, books.size());
            LOG.warn(record.getMessage());
        }
        return books;
    }

    @Override
    public boolean takeBook(Book book) {
        LOG.info("takeBook method");
        BookBO bookBO = new BookBO();
        if (!bookBO.takeNewBook(book)) {
            RecordNotFoundException record = new RecordNotFoundException(ErrorMessage.ELECT_BOOK_ALREADY_EXIST, book.getTitle());
            LOG.warn(record.getMessage());
            // throw new ServiceException(record);
        }
        LOG.info("takeBook result:" + true);
        return true;
    }
}
