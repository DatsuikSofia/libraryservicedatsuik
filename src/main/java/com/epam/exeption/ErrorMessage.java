package com.epam.exeption;

public enum ErrorMessage {
    NO_BOOK_WITH_THIS_TITLE("There is no book with  title [%s]"),
    NO_BOOK_THIS_AUTHOR_WITH_THIS_TITLE("There is no book this author with this title [%s]"),
    NOT_ENOUGH_BOOKS_OF_AUTHOR("There are only [%d] books of [%s], [%d] have bean required"),
    ELECT_BOOK_ALREADY_EXIST("[%s] book already exist");

    private String message;

    ErrorMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
