package com.epam.exeption;

public class RecordNotFoundException extends RuntimeException {
    private static final String DEFAULT_MESSAGE = "Record is not found in file";
    private String message;

    public RecordNotFoundException() {
        super(DEFAULT_MESSAGE);
    }

    public RecordNotFoundException(String message) {
        super(message);
    }

    public RecordNotFoundException(Throwable cause) {
        super(DEFAULT_MESSAGE, cause);
    }

    public RecordNotFoundException(String message, Throwable cause) {
        super(message, cause); }

    public RecordNotFoundException(ErrorMessage errorMessage, Object... args) {
        setMessage(String.format(errorMessage.getMessage(), args)); }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}