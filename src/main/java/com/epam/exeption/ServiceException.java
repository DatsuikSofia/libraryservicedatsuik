package com.epam.exeption;

public class ServiceException extends Throwable {
    private RecordNotFoundException exception;

    public ServiceException(RecordNotFoundException exception) {
        this.exception = exception;
    }

    public RecordNotFoundException getException() {
        return exception;
    }

    public void setException(RecordNotFoundException exception) {
        this.exception = exception;
    }
}
